<?php

namespace App\Repositories\Legacy;

use App\Repositories\LegacyRepository;
use Illuminate\Support\Collection;
use PDO;

/**
 * Class UserRepository
 *
 * @property int      $id
 */
class UserRepository extends LegacyRepository
{
    private const TABLE = 'users';

    /**
     * @param int $offset
     * @param int $limit
     * @return Collection
     */
    public function findLegacyByFilters(int $offset, int $limit): Collection
    {
        $tableName = self::TABLE;

        $query = <<<SQL
SELECT * FROM $tableName;
SQL;

        $statement = $this->connection->query($query);
        return $this->all($statement);

        // TODO: load models, with Eloquent
//        return LegacyUser::query()
//            ->select(LegacyUser::MIGRATE_FIELDS)
//            ->with(['userRole', 'userLanguage'])
//            ->whereNotNull(['email', 'firstname', 'lastname'])
//            ->offset($offset)
//            ->limit($limit)
//            ->get();
    }

    /**
     * @param $user
     * @return void
     */
    public function insert($user)
    {
        // ...
    }
}
