<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use PDO;
use PDOStatement;

/**
 * Class LegacyRepository
 *
 * @property $connection
 */
class LegacyRepository
{
    protected PDO $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
        //$this->connection->beginTransaction();
    }

    /**
     * @return PDO
     * @throws \Exception
     */
    public function getConnection(): PDO
    {
        //if(!$this->connection->inTransaction()) throw new \Exception("Not in transaction.");
        return $this->connection;
    }

    /**
     * @param PDOStatement $statement
     * @return Collection
     */
    protected function all(PDOStatement $statement): Collection
    {
        return collect($statement->fetchAll());
    }
}