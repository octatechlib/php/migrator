<?php

namespace App\Repositories;

use App\Models\Current\CurrentModel;
use Exception;
use PDO;

/**
 * @property $connection
 */
class CurrentRepository
{
    protected PDO $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param CurrentModel $model
     * @return bool
     * @throws Exception
     */
    public function insert(CurrentModel $model): bool
    {
        // TODO: model, eloquent

        $table = $model::TABLE_NAME;

        $attributes = $model->getAttributes();
        $modelKeys  = array_keys($attributes);
        $columnKeys = array_map( function ($property) use ($model) { return $model->camelToSnake($property); }, $modelKeys);

        $columns = implode(', ', $columnKeys);
        $params  = ':' . implode(', :', $modelKeys);

        $query = <<<SQL
INSERT INTO $table ($columns) VALUES ($params)
SQL;

        $sql = $this->connection->prepare($query);
        $values = collect($attributes);
        foreach($attributes as $attribute => $value) {
            $sql->bindParam(':' . $attribute, $value); // PDO::PARAM_INT, PDO::PARAM_STR ?
        }

        $isInserted = $sql->execute();
        if(!$isInserted) {
            throw new Exception("Unable to insert an entity");
        }

        return true;
    }
}