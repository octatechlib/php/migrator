<?php

namespace App\Console;

use App\Repositories\CurrentRepository;
use App\Repositories\LegacyRepository;
use App\Services\MigratorService;
use DI\Container;
use DI\ContainerBuilder;
use Exception;
use PDO;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:migrate',
    description: 'A migrator command.',
    aliases: ['app:migrator'],
    hidden: false
)]
/**
 * Class MigrateCommand
 */
final class MigrateCommand extends Command
{
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;
    private MigratorService    $migratorService;

    /**
     * Constructor.
     *
     * @param ContainerInterface $container The container
     * @param string|null $name The name
     */
    public function __construct(ContainerInterface $container, MigratorService $migratorService, ?string $name = null)
    {
        $this->container       = $container;
        $this->migratorService = $migratorService;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        parent::configure();

        $this->setName('app:migrate');
        $this->setDescription('A migrator command');
        $this->addArgument('type');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(sprintf('<info>Migrator 1.0</info>'));

        $type = $input->getArgument('type');
        if (empty($type)) {
            $output->writeln(sprintf('<error>Type missing. Did you mean `app:migrate users`?</error>'));
            return Command::INVALID;
        }

        $lineOne = $output->section();
        $lineTwo = $output->section();
        $lineThree = $output->section();

        $lineOne->writeln('Starting');
        $lineTwo->writeln('========');
        $lineThree->writeln($type);

        $this->migratorService->run($type);
        // TODO: return Command::FAILURE;

        sleep(1);
        $lineOne->overwrite('Done');
        sleep(1);

        return Command::SUCCESS;
    }
}
