<?php

namespace App\Migrators;

use App\Models\Current\CurrentModel;
use App\Models\Legacy\LegacyModel;

interface MigratorInterface
{
    /**
     * @param int $offset
     * @param int $limit
     *
     * @return void
     */
    public function migrate(int $offset, int $limit): void;
    public function makeLegacy(array $legacyData): LegacyModel;
    public function makeCurrent(LegacyModel $legacyModel): CurrentModel;
}
