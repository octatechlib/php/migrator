<?php

namespace App\Migrators;

use AllowDynamicProperties;
use App\Models\Current\User as CurrentUser;
use App\Models\Legacy\LegacyModel;
use App\Models\Legacy\User as LegacyUser;
use App\Repositories\CurrentRepository;
use App\Repositories\Legacy\UserRepository;
use App\Repositories\LegacyRepository;
use Exception;

/**
 *  Class UserMigrator
 *
 * @property LegacyRepository legacyRepository
 * @property CurrentRepository currentRepository
 * @property UserRepository userRepository
 */
#[AllowDynamicProperties] class UserMigrator implements MigratorInterface
{
    public function __construct(
        readonly private LegacyRepository $legacyRepository,
        readonly private CurrentRepository $currentRepository
    ) {
        $this->userRepository = new UserRepository($this->legacyRepository->getConnection());
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return void
     * @throws Exception
     */
    public function migrate(int $offset, int $limit): void
    {
        ///$this->repositoryTransaction->start();

        $legacyUsers = $this->userRepository->findLegacyByFilters($offset, $limit);

        foreach ($legacyUsers as $legacyUserData) {

            /** @var LegacyUser $legacyUser */
            // TODO: model, eloquent

            $this->currentRepository->insert(
                $this->makeCurrent(
                    $this->makeLegacy($legacyUserData)
                )
            );

            unset($authUser, $legacyUser, $role);
        }

        ///$this->repositoryTransaction->commit();

        unset($this->userRepository);
    }


    /**
     * @param LegacyUser $legacyModel
     * @return CurrentUser
     */
    public function makeCurrent(LegacyModel $legacyModel): CurrentUser
    {
        // TODO: mapper for $legacyModel->getAttributes()
        $currentModel = new CurrentUser([
            'first_name' => $legacyModel->getFirstName(),
            'last_name'  => $legacyModel->getInsertion()
                ? $legacyModel->getInsertion() . ' ' . $legacyModel->getLastName()
                : $legacyModel->getLastname(),
        ]);

        return $currentModel;
    }


    /**
     * @param array $legacyData
     * @return LegacyUser
     */
    public function makeLegacy(array $legacyData = []): LegacyUser
    {
        $only = LegacyUser::MIGRATE_FIELDS;
        $filteredData = array_filter($legacyData, fn ($key) => in_array($key, $only), ARRAY_FILTER_USE_KEY);
        return new LegacyUser($filteredData);
    }
}
