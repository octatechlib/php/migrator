<?php

namespace App\Models;

abstract class Model
{
    /**
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        foreach($properties as $property => $value) {
            $this->mapProperty($property, $value);
        }
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param int|string $property
     * @param mixed $value
     * @return void
     */
    private function mapProperty(int|string $property, mixed $value)
    {
        // TODO: ugly, adapter?
        if (property_exists($this, $property)) {
            $this->{$property} = $value;
        } else if (property_exists($this, $this->snakeToCamel($property))) {
            $camelProperty = $this->snakeToCamel($property);
            $this->{$camelProperty} = $value;
        }
    }

    /**
     * @param $input
     * @return string
     */
    function camelToSnake($input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    /**
     * @param $input
     * @return string
     */
    function snakeToCamel($input): string
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $input))));
    }
}