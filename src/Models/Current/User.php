<?php

namespace App\Models\Current;

use App\Models\Model;

/**
 * Class User
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 */
class User extends Model implements CurrentModel
{
    const TABLE_NAME = 'users';

    protected $firstName;
    protected $lastName;
    protected $email;
}