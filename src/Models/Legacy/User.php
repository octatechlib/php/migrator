<?php

namespace App\Models\Legacy;

use App\Models\Model;

class User extends Model implements LegacyModel
{
    const TABLE_NAME = 'users';
    public final const MIGRATE_FIELDS = [
        'firstname',
        'insertion',
        'lastname',
    ];

    protected $firstname;
    protected $insertion;
    protected $lastname;


    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return mixed
     */
    public function getInsertion()
    {
        return $this->insertion;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

}
