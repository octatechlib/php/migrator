<?php

namespace App\Services;

use App\Migrators\MigratorInterface;
use App\Repositories\CurrentRepository;
use App\Repositories\LegacyRepository;
use Exception;

/**
 * Class MigratorService
 */
readonly class MigratorService
{
    /**
     * @param LegacyRepository $legacyRepository
     * @param CurrentRepository $currentRepository
     * @param array $migratorClasses
     */
    public function __construct(
        public LegacyRepository  $legacyRepository,
        public CurrentRepository $currentRepository,
        public array $migratorClasses
    )
    {}

    /**
     * @param $type
     * @param $offset
     * @param $limit
     * @return void
     * @throws Exception
     */
    public function run($type, $offset = 0, $limit = 100)
    {
        if(isset($this->migratorClasses[$type])) {
            /** @var MigratorInterface $migrator */
            $migrator =  new $this->migratorClasses[$type]($this->legacyRepository, $this->currentRepository);

            if(! ($migrator instanceof MigratorInterface)) {
                throw new Exception('No migrator class found for ' . $type);
            }

            $migrator->migrate($offset, $limit);
        }

    }
}