# Migrator

[![Run](https://img.shields.io/badge/Local-8080-red.svg)](http://localhost:8080)
[![Run](https://img.shields.io/badge/PHPMyAdmin-source-dark_green.svg)](http://10.1.0.200/index.php?route=/&route=%2F&db=legacy_database&table=users)
[![Run](https://img.shields.io/badge/PHPMyAdmin-target-yellow.svg)](http://10.1.0.200/index.php?route=/&route=%2F&db=current_database&table=users)


### Details

[![State badge](https://img.shields.io/badge/Release-alpha-orange.svg)](https://gitlab.com/paydia_2_1/payroll-service)
[![Version badge](https://img.shields.io/badge/Version-x.x-blue.svg)](https://gitlab.com/paydia_2_1/payroll-service)
[![Last stable badge](https://img.shields.io/badge/Last_stable-x.x-green.svg)](https://gitlab.com/paydia_2_1/payroll-service)
[![Licence badge](https://img.shields.io/badge/Licence-GPL_octatech-1f425f.svg)](http://octatech.nl/)

## Contains services
Migrator app is a console application to migrate data from one database to the other.
* http://10.1.0.200/ - PHPMyAdmin
* `10.1.0.110` - legacy host (source)
   * db: `legacy_database`
   * user: `legacy_user`
   * password: `legacy_pass`
* `10.1.0.120` - current host (target)
  * db: `current_database`
  * user: `current_user`
  * password: `current_pass`

## Install the Application

It's a console application. Remember about:
```bash
chmod +x bin/console.php
```
## Start the Application
```bash
docker compose up -d
```

## Use the Application
Use `php bin/console.php migrate` with params to migrate a model.

How to run locally:
```bash
make migrate users
```

How to run from the container:
```bash
php bin/console.php app:migrate users
```

After that, open `http://localhost:8080` in your browser.

Run this command in the application directory to run the test suite

```bash
php8.2-intl
```

Enjoy

