FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Amsterdam

ARG USER_ID=1000
ARG GROUP_ID=1000

USER root

RUN mkdir -p /var/www/
RUN chmod 777 /var/www/

WORKDIR /var/www/

# Create a non-root user
RUN addgroup --gid $GROUP_ID www && \
    adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID www && \
    passwd -d www && \
    echo 'www ALL=(ALL:ALL) NOPASSWD: ALL' > /etc/sudoers

ARG PHP_CLI_DIR="/etc/php/7.4/cli"
ARG PHP_FPM_DIR="/etc/php/7.4/fpm"

RUN apt update
RUN apt install -y \
    curl \
    git \
    locales \
    software-properties-common \
    vim \
    xz-utils \
    wget

RUN locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en

RUN apt-add-repository ppa:ondrej/php
RUN apt update

RUN apt install --no-install-recommends php8.2 -y
RUN apt install -y \
    php8.2-fpm \
    php8.2-dom \
    php8.2-xml \
    php8.2-xmlwriter \
    php8.2-curl \
    php8.2-mysql \
    php8.2-pdo \
    php8.2-sqlite3

# composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#RUN chmod +x /usr/bin/composer

COPY /config/env.example.php /config/env.local.php

EXPOSE 8080/tcp

CMD /etc/init.d/php8.2-fpm start -F && php -S 0.0.0.0:8080 -t public
