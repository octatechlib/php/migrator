<?php

$_ENV['APP_ENV'] = 'local';

$_ENV['LEGACY_DB_CONNECTION'] = 'legacy';
$_ENV['LEGACY_DB_HOST'] = '10.1.0.110';
$_ENV['LEGACY_DB_PORT'] = '3306';
$_ENV['LEGACY_DB_DATABASE'] = 'legacy_db';
$_ENV['LEGACY_DB_USERNAME'] = 'legacy_user';
$_ENV['LEGACY_DB_PASSWORD'] = 'legacy_pass';

$_ENV['CURRENT_DB_CONNECTION'] = 'current';
$_ENV['CURRENT_DB_HOST'] = '10.1.0.120';
$_ENV['CURRENT_DB_PORT'] = '3306';
$_ENV['CURRENT_DB_DATABASE'] = 'current_db';
$_ENV['CURRENT_DB_USERNAME'] = 'current_user';
$_ENV['CURRENT_DB_PASSWORD'] = 'current_pass';
