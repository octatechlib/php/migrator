<?php

use App\Migrators\UserMigrator;

return [
    'settings' => [
        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails' => true,
    ],
    'migrators' => [
        'users' => UserMigrator::class,
    ],
    'db_legacy' => [
        'driver' => 'mysql',
        'host' => '10.1.0.110',
        'database' => 'legacy_database',
        'username' => 'root',
        'password' => 'secret',
//        'username' => 'legacy_user',
//        'password' => 'legacy_password',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ],
    'db_current' => [
        'driver' => 'mysql',
        'host' => '10.1.0.120',
        'database' => 'current_database',
        'username' => 'root',
        'password' => 'secret',
//        'username' => 'current_user',
//        'password' => 'current_password',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ],
];
