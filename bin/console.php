<?php

use App\Application\Settings\SettingsInterface;
use App\Console\MigrateCommand;
use App\Repositories\CurrentRepository;
use App\Repositories\LegacyRepository;
use App\Services\MigratorService;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;

require_once __DIR__ . '/../vendor/autoload.php';

$env = (new ArgvInput())->getParameterOption(['--env', '-e'], 'dev');

if ($env) {
    $_ENV['APP_ENV'] = $env;
}

try {
    /** @var ContainerInterface $container */
    $container = (new ContainerBuilder())
        ->addDefinitions(__DIR__ . '/../config/container.php')
        ->addDefinitions([
        MigratorService::class => function (ContainerInterface $c) {
            $leg = $c->get('db_legacy');
            $cur = $c->get('db_current');

            $migratorClasses = $c->get('migrators');

            $legacyDns  = 'mysql:host=' . $leg['host'] . ';dbname=' . $leg['database'] . ';charset=' . $leg['charset'];
            $currentDns = 'mysql:host=' . $cur['host'] . ';dbname=' . $cur['database'] . ';charset=' . $cur['charset'];

            $legacyPdo = new PDO($legacyDns, $leg['username'], $leg['password'], [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]);
            $legacyPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $currentPdo = new PDO($currentDns, $cur['username'], $cur['password'], [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]);
            $currentPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return new MigratorService(
                new LegacyRepository($legacyPdo),
                new CurrentRepository($currentPdo),
                $migratorClasses
            );
        },
        LegacyRepository::class => function (ContainerInterface $c) {
            $leg  = $c->get('db_legacy');

            $legacyDns  = 'mysql:host=' . $leg['host'] . ';dbname=' . $leg['database'] . ';charset=' . $leg['charset'];

            $legacyPdo = new PDO($legacyDns, $leg['username'], $leg['password']/**, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]**/);
            $legacyPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return new LegacyRepository($legacyPdo);
        },
        CurrentRepository::class => function (ContainerInterface $c) {
            $cur  = $c->get('db_current');

            $currentDns = 'mysql:host=' . $cur['host'] . ';dbname=' . $cur['database'] . ';charset=' . $cur['charset'];

            $currentPdo = new PDO($currentDns, $cur['username'], $cur['password']/**, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]**/);
            $currentPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return new CurrentRepository($currentPdo);
        }
    ])->build();

    /** @var Application $application */
    $application = $container->get(Application::class);

    // Register your console commands here
    $application->add($container->get(MigrateCommand::class));

    exit($application->run());
} catch (Throwable $exception) {
    echo $exception->getMessage();
    exit(1);
}
