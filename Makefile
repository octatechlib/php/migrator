help:	  ## Show this help
	@echo ""
	@echo "Usage:  make COMMAND"
	@echo ""
	@echo "Commands:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
.PHONY: help

MIGRATOR_CONTAINER := $(shell docker ps --format "{{.Names}}" | grep migrator-migrator-)

build:	  ## (Re)build locally the docker containers
	docker compose build
.PHONY: build

down:	  ## Stop and remove the docker containers
	docker compose down
.PHONY: down

stop:	  ## Stop the docker containers
	docker stop $(MIGRATOR_CONTAINER)
.PHONY: stop

start:	  ## Start the docker containers
	docker start $(MIGRATOR_CONTAINER)
.PHONY: start

tail:	  ## Tail the log files of the containers
	docker logs -f -t --tail=20 $(MIGRATOR_CONTAINER)
.PHONY: tail

up:       ## Start the docker containers for local development
	docker compose up -d
.PHONY: up

in:       ## Go inside of main container
	docker exec -it $(MIGRATOR_CONTAINER) /bin/bash
.PHONY: in

migrate: ## Run migrate command with a table, e.g. make migrate users
	docker exec -it $(MIGRATOR_CONTAINER) php bin/console.php app:migrate $(filter-out $@, $(MAKECMDGOALS))
.PHONY: migrate
